CREATE TABLE single_transactions (
  id serial PRIMARY KEY,
  type VARCHAR NOT NULL,
  amount numeric,
  created_at timestamp with time zone NOT NULL,
  description TEXT
);

-- Audit purposes
CREATE TABLE draft_transactions (
  id serial PRIMARY KEY,
  type VARCHAR NOT NULL,
  amount numeric,
  created_at timestamp with time zone NOT NULL,
  description TEXT,
  msg TEXT
  -- ,approved_id INTEGER,

  -- FOREIGN KEY (approved_id) REFERENCES single_transaction (id)
);