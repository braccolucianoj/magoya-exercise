-- Crea rol postgres_expenses que será el owner de la base de datos
CREATE ROLE postgres_expenses NOSUPERUSER NOCREATEDB NOCREATEROLE NOINHERIT LOGIN ENCRYPTED PASSWORD 'expensePassword';

-- Crea la base de datos
CREATE DATABASE expenses WITH OWNER = postgres_expenses;

REVOKE ALL PRIVILEGES ON DATABASE expenses FROM public;
REVOKE ALL PRIVILEGES ON SCHEMA pg_catalog FROM public;
REVOKE ALL PRIVILEGES ON SCHEMA information_schema FROM public;
REVOKE CONNECT ON DATABASE expenses FROM public;
GRANT CONNECT ON DATABASE expenses to postgres_expenses;

GRANT ALL PRIVILEGES ON DATABASE expenses TO postgres_expenses;
GRANT ALL PRIVILEGES ON DATABASE expenses TO postgres_expenses;
