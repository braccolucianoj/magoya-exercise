import React from 'react';
import './App.css';
import { APIProvider } from './providers/api.provider';
import TransactionPage from './pages/Transaction.page';

const App = () => (
    <APIProvider>
        {/* If there were more pages, then I would a router */}
        <TransactionPage />
    </APIProvider>
);

export default App;
