import axios from 'axios';

const getURL = () => {
    const { REACT_APP_ENVIRONMENT: environment } = process.env;
    switch (environment) {
        case 'container':
            return `http://backend:8000`;
        default:
            return 'http://localhost:8000';
    }
};

const BASE_URL = getURL();

export const getTransactions = async ({ page, limit }) => {
    const { data } = await axios.get(
        `${BASE_URL}/transactions?page=${page || 1}&limit=${limit || 10}`
    );
    const { total_pages, ...response } = data;
    return { ...response, totalPages: total_pages };
};

export const createTransaction = async (transaction) => {
    const { type, ...rest } = transaction;
    const { data } = await axios.post(`${BASE_URL}/transactions`, {
        ...rest,
        t_type: type,
    });
    return data;
};

export const getBalance = async () => {
    const { data } = await axios.get(`${BASE_URL}/balance`);
    return data;
};
