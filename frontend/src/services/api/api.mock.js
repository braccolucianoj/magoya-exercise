export const TRANSACTION_DEBIT_TYPE = 'debit';
export const TRANSACTION_CREDIT_TYPE = 'credit';

export const transactions = [
    {
        id: 1,
        amount: 12,
        type: TRANSACTION_DEBIT_TYPE,
        description: 'Random',
        createdAt: new Date(0),
    },
    {
        id: 2,
        amount: 12,
        type: TRANSACTION_DEBIT_TYPE,
        description: 'Random',
        createdAt: new Date(100000),
    },
    {
        id: 3,
        amount: 1,
        type: TRANSACTION_CREDIT_TYPE,
        description: 'Random',
        createdAt: new Date(Date.now()),
    },
];

export const getTransactions = async ({ page = 0, limit = 100 }) =>
    new Promise((resolve) =>
        setTimeout(
            () =>
                resolve({
                    transactions: transactions.slice(0, limit),
                    total: transactions.length,
                }),
            1000
        )
    );

export const createTransaction = async (transaction) => ({
    id: 123,
    createdAt: new Date(Date.now()),
    ...transaction,
});

export const getBalance = async () =>
    new Promise((resolve) =>
        setTimeout(
            () =>
                resolve({
                    balance: 123,
                }),
            1000
        )
    );
