import * as APIService from './api.service';
import * as APIServiceMockImport from './api.mock';

export default { ...APIService };
export const APIServiceMock = APIServiceMockImport;
