import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import { APIContext } from '../providers/api.provider';
import { Layout, notification } from 'antd';
import Header from '../components/Header';
import TransactionsList from '../components/TransactionsList';
import CreateTransaction from '../components/CreateTransaction';

const { Content } = Layout;

const LIMIT = 5;

const notificateCreation = (transaction, onClose) => {
    notification.success({
        message: `Success`,
        description: `The transaction (${transaction.type} type) has value of ${transaction.amount}`,
        duration: 2,
        onClose: onClose(),
    });
};

const notificateError = (err, transaction) => {
    notification.error({
        message: 'Error!',
        description: `An error occured while trying to create the transaction (${transaction.type} type) with value of ${transaction.amount}: ${err.description}`,
        duration: 2,
    });
};

export const TransactionPageBare = ({ APIService }) => {
    const [transactions, setTransactions] = useState([]);
    const [page, setPage] = useState(1);
    const [total, setTotal] = useState(0);
    const [loading, setLoading] = useState(true);
    const [showModal, setShowModal] = useState(false);

    const reloadTransactions = async (page, limit) => {
        setLoading(true);
        const {
            transactions: transactionList,
            total,
            totalPages,
        } = await APIService.getTransactions({
            page,
            limit,
        });
        setTransactions(transactionList);
        setTotal(total);
        setLoading(false);
    };

    useEffect(() => {
        reloadTransactions(page, LIMIT);
    }, [page]);

    return (
        <>
            {showModal && (
                <CreateTransaction
                    APIService={APIService}
                    onCancel={() => setShowModal(false)}
                    onCreation={(transaction) => {
                        notificateCreation(transaction, () =>
                            reloadTransactions(page, LIMIT)
                        );
                        setShowModal(false);
                    }}
                    onError={notificateError}
                />
            )}
            <Layout>
                <Header
                    data-testid="header"
                    page={page}
                    total={total}
                    onCreateClick={() => setShowModal(true)}
                    onRefresh={() => reloadTransactions(page, LIMIT)}
                />
                <Content>
                    <TransactionsList
                        data-testid="transactionList"
                        loading={loading}
                        transactions={transactions}
                        page={page}
                        pageSize={LIMIT}
                        onPageChange={(page) => setPage(page)}
                        total={total}
                    />
                </Content>
            </Layout>
        </>
    );
};

export default () => {
    const { service } = useContext(APIContext);
    return <TransactionPageBare APIService={service} />;
};
