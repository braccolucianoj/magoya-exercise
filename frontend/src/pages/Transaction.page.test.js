import React from 'react';
import { render, cleanup } from '@testing-library/react';

import { TransactionPageBare } from './Transaction.page';
import { getTransactions } from '../services/api/api.mock';

afterEach(cleanup);

describe('Testing Transaction Page', () => {
    beforeAll(() => {
        Object.defineProperty(window, 'matchMedia', {
            value: jest.fn(() => {
                return {
                    matches: false,
                    addListener: function () {},
                    removeListener: function () {},
                };
            }),
        });
    });

    it('Renders correctly on creation', async () => {
        const getTransactionsSpy = jest.fn(getTransactions);
        const { getByText } = render(
            <TransactionPageBare
                APIService={{
                    getTransactions: getTransactionsSpy,
                }}
            />
        );
        expect(getTransactionsSpy).toHaveBeenCalled();
        expect(getByText('Transactions List')).toBeDefined();
    });
});
