import React, { useState, createContext } from 'react';
import APIService, { APIServiceMock } from '../services/api';

export const APIContext = createContext({});

const loadService = () => {
    const { REACT_APP_ENVIRONMENT: environment } = process.env;
    switch (environment) {
        case 'standalone':
            return APIServiceMock;
        default:
            return APIService;
    }
};

export const APIProvider = ({
    value: contextValue = loadService(),
    children,
}) => {
    const [value, setValue] = useState(contextValue);

    return (
        <APIContext.Provider
            value={{ updateService: setValue, service: value }}
        >
            {children}
        </APIContext.Provider>
    );
};
