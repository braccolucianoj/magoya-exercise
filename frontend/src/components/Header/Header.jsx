import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import { APIContext } from '../../providers/api.provider';
import { Row, Col, Button, Divider } from 'antd';
import { PlusOutlined, ReloadOutlined } from '@ant-design/icons';
import './Header.css';

export const HeaderBare = ({ APIService, total, onRefresh, onCreateClick }) => {
    const [balance, setBalance] = useState(0);
    const [loading, setLoading] = useState(true);
    const [refreshLoading, setRefreshLoading] = useState(false);

    const getBalance = async () => {
        setLoading(true);
        try {
            const balanceGet = await APIService.getBalance();
            setBalance(balanceGet.balance);
        } catch (err) {}
        setLoading(false);
    };

    const onRefreshClick = async () => {
        setRefreshLoading(true);
        try {
            await Promise.all([onRefresh(), getBalance()]);
        } catch (err) {}
        setRefreshLoading(false);
    };

    useEffect(() => {
        getBalance();
    }, []);

    return (
        <>
            <Row>
                <Col span={24}>Transactions List</Col>
            </Row>
            <Divider orientation="left" className="divider" />
            <Row>
                <Col span={12}>Actual balance {balance}</Col>
            </Row>
            <Row>
                <Col span={22}>Total transactions {total}</Col>
                <Col span={1}>
                    <Button
                        id="refresh-button"
                        disabled={refreshLoading || loading}
                        onClick={onRefreshClick}
                        loading={refreshLoading}
                    >
                        <ReloadOutlined />
                    </Button>
                </Col>
                <Col span={1}>
                    <Button
                        id="create-button"
                        disabled={refreshLoading || loading}
                        onClick={onCreateClick}
                    >
                        <PlusOutlined />
                    </Button>
                </Col>
            </Row>
        </>
    );
};

export const Header = (props) => {
    const { service } = useContext(APIContext);
    return <HeaderBare APIService={service} {...props} />;
};
