import React from 'react';
import { render, cleanup, fireEvent, act } from '@testing-library/react';

import { HeaderBare } from './Header';

afterEach(cleanup);

describe('Testing Header Behaviour', () => {
    beforeAll(() => {
        Object.defineProperty(window, 'matchMedia', {
            value: jest.fn(() => {
                return {
                    matches: false,
                    addListener: function () {},
                    removeListener: function () {},
                };
            }),
        });
    });

    it('Renders correctly on creation and closes and calls createTransaction & onCancel', async () => {
        const getBalanceSpy = jest.fn(() => Promise.resolve({ balance: 10 }));
        const spyOnRefresh = jest.fn(() => Promise.resolve());
        const spyOnCreateClick = jest.fn();
        const total = 10;
        const { container, getByText } = render(
            <HeaderBare
                APIService={{
                    getBalance: getBalanceSpy,
                }}
                total={total}
                onRefresh={spyOnRefresh}
                onCreateClick={spyOnCreateClick}
            />
        );
        expect(getBalanceSpy).toHaveBeenCalled();
        await act(getBalanceSpy);

        expect(getByText('Transactions List')).toBeDefined();
        expect(
            getByText(new RegExp(`Total transactions ${total}`))
        ).toBeDefined();

        fireEvent.click(container.querySelector('#create-button'), {
            button: 0,
        });
        expect(spyOnCreateClick).toHaveBeenCalled();
        fireEvent.click(container.querySelector('#refresh-button'), {
            button: 0,
        });
        expect(getBalanceSpy).toHaveBeenCalled();
        expect(spyOnRefresh).toHaveBeenCalled();
        await act(getBalanceSpy);
        await act(spyOnRefresh);
    });
});
