import React from 'react';
import { render, cleanup } from '@testing-library/react';

import { TransactionsList } from './TransactionsList';
import { transactions } from '../../services/api/api.mock';

afterEach(cleanup);

describe('Testing Transaction List Behaviour', () => {
    beforeAll(() => {
        Object.defineProperty(window, 'matchMedia', {
            value: jest.fn(() => {
                return {
                    matches: false,
                    addListener: function () {},
                    removeListener: function () {},
                };
            }),
        });
    });

    it('Renders correctly on creation', async () => {
        const spy = jest.fn();
        const { findAllByText } = render(
            <TransactionsList
                transactions={transactions}
                page={1}
                total={10}
                pageSize={5}
                onPageChange={spy}
                loading={false}
            />
        );
        await Promise.all(
            transactions.map(async (t) => {
                expect(
                    (await findAllByText(t.id.toString())).length
                ).toBeGreaterThanOrEqual(1);
                expect(
                    (await findAllByText(t.createdAt.toString())).length
                ).toBeGreaterThanOrEqual(1);
                expect(
                    (await findAllByText(t.amount.toString())).length
                ).toBeGreaterThanOrEqual(1);
                expect(
                    (await findAllByText(t.description)).length
                ).toBeGreaterThanOrEqual(1);
                expect(
                    (await findAllByText(t.type)).length
                ).toBeGreaterThanOrEqual(1);
            })
        );
        // fireEvent.click(
        //     document.getElementsByClassName('ant-pagination-item-1'),
        //     {
        //         button: 0,
        //     }
        // );
        // expect(spy).toHaveBeenCalled();
        // expect(spyOnRefresh).toHaveBeenCalled();
    });
});
