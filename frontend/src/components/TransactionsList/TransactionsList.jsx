import React from 'react';
import { Table } from 'antd';
import './TransacionsList.css';

const columns = [
    {
        title: 'Id',
        dataIndex: 'id',
        sorter: {
            compare: (a, b) => a.created_at - b.created_at,
            multiple: 2,
        },
    },
    {
        title: 'Date',
        dataIndex: 'createdAt',
        sorter: {
            compare: (a, b) => a.createdAt - b.createdAt,
            multiple: 1,
        },
    },
    {
        title: 'Amount',
        dataIndex: 'amount',
    },
    {
        title: 'Description',
        dataIndex: 'description',
    },
    {
        title: 'Type',
        dataIndex: 'type',
    },
];

export const TransactionsList = ({
    transactions,
    page,
    total,
    pageSize,
    onPageChange,
    loading,
}) => {
    const data = transactions.map((transaction) => ({
        ...transaction,
        createdAt: transaction.createdAt.toString(),
        className: transaction.type,
    }));

    return (
        <Table
            loading={loading}
            columns={columns}
            dataSource={data}
            rowKey={(record) => record.id}
            rowClassName={(record) => record.className}
            pagination={{
                current: page,
                defaultCurrent: page,
                total,
                pageSize,
                onChange: onPageChange,
            }}
        />
    );
};
