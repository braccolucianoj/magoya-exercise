import React, { useState } from 'react';
import { Modal, Button, Input, InputNumber, Select, Row, Col } from 'antd';
const { Option } = Select;

const TRANSACTION_TYPES = ['credit', 'debit'];

export const CreateTransaction = ({
    APIService,
    onCancel,
    onCreation,
    onError,
}) => {
    const [description, setDescription] = useState();
    const [type, setType] = useState(TRANSACTION_TYPES[0]);
    const [amount, setAmount] = useState();

    const handleOk = async () => {
        const transactionBare = {
            description,
            type,
            amount: Number(amount),
        };
        try {
            const transaction = await APIService.createTransaction(
                transactionBare
            );
            onCreation(transaction);
        } catch (err) {
            onError(err, transactionBare);
        }
    };

    const onSetAmount = (value) => {
        if (value >= 0) {
            setAmount(value);
        }
    };

    return (
        <Modal
            id="create-modal"
            title="Create Transaction"
            visible={true}
            onOk={handleOk}
            onCancel={onCancel}
            footer={[
                <Button key="back" onClick={onCancel}>
                    Cancel
                </Button>,
                <Button
                    disabled={!(description && type && amount)}
                    key="submit"
                    type="primary"
                    onClick={handleOk}
                >
                    Add
                </Button>,
            ]}
        >
            <Row>
                <Col span={8}>
                    <label htmlFor="amount">Amount</label>
                </Col>
                <Col span={16}>
                    <InputNumber
                        id="amount"
                        name="amount"
                        value={amount}
                        onChange={onSetAmount}
                    />
                </Col>
            </Row>
            <Row>
                <Col span={8}>
                    <label htmlFor="type">Type</label>
                </Col>
                <Col span={16}>
                    <Select id="type" defaultValue={type} onChange={setType}>
                        {TRANSACTION_TYPES.map((type) => (
                            <Option key={type} value={type}>
                                {type}
                            </Option>
                        ))}
                    </Select>
                </Col>
            </Row>
            <Row>
                <Col span={8}>
                    <label htmlFor="description">Description</label>
                </Col>
                <Col span={16}>
                    <Input
                        id="description"
                        name="description"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    />
                </Col>
            </Row>
        </Modal>
    );
};
