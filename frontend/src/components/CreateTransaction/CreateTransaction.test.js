import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';

import { CreateTransaction } from './CreateTransaction';
import { TRANSACTION_CREDIT_TYPE } from '../../services/api/api.mock';

afterEach(cleanup);

describe('Testing Create Transaction Behaviour', () => {
    beforeAll(() => {
        Object.defineProperty(window, 'matchMedia', {
            value: jest.fn(() => {
                return {
                    matches: false,
                    addListener: function () {},
                    removeListener: function () {},
                };
            }),
        });
    });

    it('Renders correctly on creation and closes and calls createTransaction & onCancel', async () => {
        const spyCreateTransaction = jest.fn();
        const spyOnCancel = jest.fn();
        const spyOnCreation = jest.fn();
        const expected = {
            amount: 12.01,
            type: TRANSACTION_CREDIT_TYPE,
            description: 'test',
        };
        const { getByLabelText, getByText } = render(
            <CreateTransaction
                APIService={{
                    createTransaction: spyCreateTransaction,
                }}
                onCreation={spyOnCreation}
                onError={jest.fn()}
                onCancel={spyOnCancel}
            />
        );
        expect(getByText('Create Transaction')).toBeDefined();
        fireEvent.change(getByLabelText(/amount/i), {
            target: { value: expected.amount.toString() },
        });
        fireEvent.change(getByLabelText(/type/i), {
            target: { value: expected.type },
        });
        fireEvent.change(getByLabelText(/description/i), {
            target: { value: expected.description },
        });

        fireEvent.click(getByText(/add/i), { button: 0 });
        expect(spyCreateTransaction).toHaveBeenCalled();
        await expect(spyCreateTransaction).resolves;
        expect(spyCreateTransaction).toHaveBeenCalledWith(expected);
        expect(spyOnCreation).toHaveBeenCalled();
        expect(spyOnCancel).not.toHaveBeenCalled();
    });

    it('Renders correctly on close and call onCancel prop', () => {
        const spyOnCancel = jest.fn();
        const spyOnCreation = jest.fn();
        const spyOnError = jest.fn();
        const { getByText } = render(
            <CreateTransaction
                APIService={{
                    createTransaction: jest.fn(),
                }}
                onCancel={spyOnCancel}
                onCreation={spyOnCreation}
                onError={spyOnError}
            />
        );
        fireEvent.click(getByText(/cancel/i), { button: 0 });
        expect(spyOnCancel).toHaveBeenCalled();
        expect(spyOnCreation).not.toHaveBeenCalled();
        expect(spyOnError).not.toHaveBeenCalled();
    });

    it('Renders correctly on error calls onError prop', () => {
        const spy = jest.fn(() => {
            throw Exception();
        });
        const expected = {
            amount: 12.01,
            type: TRANSACTION_CREDIT_TYPE,
            description: 'test',
        };
        const spyOnCancel = jest.fn();
        const spyOnCreation = jest.fn();
        const spyOnError = jest.fn();
        const { getByLabelText, getByText } = render(
            <CreateTransaction
                APIService={{
                    createTransaction: spy,
                }}
                onCancel={spyOnCancel}
                onCreation={spyOnCreation}
                onError={spyOnError}
            />
        );
        fireEvent.change(getByLabelText(/amount/i), {
            target: { value: expected.amount.toString() },
        });
        fireEvent.change(getByLabelText(/type/i), {
            target: { value: expected.type },
        });
        fireEvent.change(getByLabelText(/description/i), {
            target: { value: expected.description },
        });
        fireEvent.click(getByText(/add/i), { button: 0 });
        expect(spy).toHaveBeenCalled();
        expect(spyOnError).toHaveBeenCalled();
        expect(spyOnCreation).not.toHaveBeenCalled();
        expect(spyOnCancel).not.toHaveBeenCalled();
    });
});
