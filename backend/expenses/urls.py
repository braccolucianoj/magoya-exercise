from apps.transactions.urls import urlpatterns as tx_urls

urlpatterns = tx_urls
# urlpatterns = [] + tx_urls # Expressed this way if we have more apps we can add them

from django.urls import get_resolver
get_resolver().reverse_dict.keys()
