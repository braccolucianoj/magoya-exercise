#!/bin/bash
echo "Installing pip3"
sudo apt-get install python3-pip libpq-dev python-dev
echo "Installing virtualenv"
pip3 install --user virtualenv
echo "Changing directory to src"
cd src
echo "Creating env directory"
virtualenv -p python3.6 env
