import json
from django.db import models
from functools import reduce
from django.core.exceptions import ValidationError
from datetime import datetime
from generics.managers import GenericManager
from django.core.paginator import Paginator

CREDIT = 'credit'
DEBIT = 'debit'
TRANSACTION_TYPE = [
    (CREDIT, 'credit'),
    (DEBIT, 'debit'),
]


class TransactionManager(GenericManager):
    def get_balance(self):
        return reduce(lambda x, y: x + y.amount_value, self.all(), 0)

    def check_valid(self, tx):
        if tx.amount <= 0:
            msg = f'Transction with amount less or equal than zero'
            raise ValidationError(msg)
        if tx.t_type == DEBIT:
            balance = self.get_balance()
            if balance < tx.amount:
                msg = f'DEBIT Transction with amount greater than balance ${balance}'
                draft = DraftTransaction({**tx.to_dict(), 'msg': msg})
                raise ValidationError(msg)

    def list(self, page, limit):
        transactions = self.all().order_by('-created_at')
        paginator = Paginator(transactions, limit)
        return [paginator.page(page), paginator.count, paginator.num_pages]


class Transaction(models.Model):
    id = models.AutoField(primary_key=True, editable=False, )
    t_type = models.CharField(
        max_length=20, db_column='type', choices=TRANSACTION_TYPE, default=DEBIT)
    amount = models.DecimalField(max_digits=10, decimal_places=3)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    description = models.TextField(blank=True)

    @property
    def amount_value(self):
        return (1 if self.t_type == CREDIT else -1) * self.amount

    objects = TransactionManager()

    def to_dict(self):
        return self.__dict__

    class Meta:
        db_table = 'single_transactions'
        managed = False


class DraftTransaction(models.Model):
    t_type = models.CharField(
        max_length=20, db_column='type', choices=TRANSACTION_TYPE, default=DEBIT)
    amount = models.DecimalField(max_digits=10, decimal_places=3)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    description = models.TextField()
    msg = models.TextField()

    class Meta:
        db_table = 'single_transactions'
        managed = False
