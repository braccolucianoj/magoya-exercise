from django.test import TestCase
from apps.transactions.models import CREDIT, DEBIT, Transaction
# request = factory.post('/notes/', {'title': 'new idea'})
from rest_framework.test import APIClient
from decimal import Decimal, getcontext

getcontext().prec = 3

TX_CREDIT = {'t_type': CREDIT, 'description': 'Random', 'amount': Decimal(12)}
TX_DEBIT = {'t_type': DEBIT, 'description': 'Random', 'amount': Decimal(10.1)}
TX_DEBIT_LARGE = {'t_type': DEBIT,
                  'description': 'Random', 'amount': Decimal(120)}


class TestingTransactionViews(TestCase):

    def setUp(self):
        self.client = APIClient()
        Transaction.objects.truncate()

    def _assert_correct_balance(self, response, balance_value):
        self.assertEqual(response.status_code, 200)
        self.assertEqual((response.data['balance']), balance_value)

    def _assert_correct_tx(self, response, tx_input):
        self.assertEqual(response.status_code, 201)
        copy = tx_input.copy()
        tx_type = copy.pop('t_type')
        self.assertDictContainsSubset(
            {**copy, 'type': tx_type}, response.data)

    def _assert_correct_tx(self, response, tx_input):
        self.assertEqual(response.status_code, 201)
        copy = tx_input.copy()
        tx_type = copy.pop('t_type')
        self.assertDictContainsSubset(
            {**copy, 'type': tx_type}, response.data)

    def _assert_tx_list(self, response, tx_input):
        self.assertEqual(response.status_code, 201)
        copy = tx_input.copy()
        tx_type = copy.pop('t_type')
        copy['type'] = tx_type
        self.assertDictContainsSubset(copy, response.data)
        self.assertListEqual(copy, ['type', 'amount',
                                    'id', 'description', 'createdAt'])

    def testing_credit_tx(self):
        response = self.client.get('/balance', format='json')
        self._assert_correct_balance(response, 0)
        response = self.client.post('/transactions', TX_CREDIT, format='json')
        self._assert_correct_tx(response, TX_CREDIT)
        response = self.client.get('/balance', format='json')
        self._assert_correct_balance(response, TX_CREDIT['amount'])

    def testing_invalid_tx(self):
        new_amount = Decimal(-123)
        response = self.client.post(
            '/transactions', {**TX_CREDIT, 'amount': new_amount}, format='json')
        self.assertEqual(response.status_code, 400)
        response = self.client.get('/balance', format='json')
        self._assert_correct_balance(response, 0)

    def testing_credit_debit_tx(self):
        response = self.client.get('/balance', format='json')
        self._assert_correct_balance(response, 0)
        response = self.client.post('/transactions', TX_CREDIT, format='json')
        self._assert_correct_tx(response, TX_CREDIT)
        response = self.client.post('/transactions', TX_DEBIT, format='json')
        self._assert_correct_tx(response, TX_DEBIT)
        response = self.client.get('/balance', format='json')
        self._assert_correct_balance(
            response, TX_CREDIT['amount'] - TX_DEBIT['amount'])

    def testing_tx_list(self):
        self.client.post('/transactions', TX_CREDIT, format='json')
        self.client.post('/transactions', TX_CREDIT, format='json')
        self.client.post('/transactions', TX_CREDIT, format='json')
        response = self.client.get(
            '/transactions?limit=10&page=1', format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['total'], 3)
        self.assertEqual(response.data['total_pages'], 1)
        map(lambda x: self._assert_correct_tx(
            x, TX_CREDIT), response.data['transactions'])

    def testing_large_invalid_debit_tx(self):
        self.client.post('/transactions', TX_CREDIT, format='json')
        response = self.client.post(
            '/transactions', TX_DEBIT_LARGE, format='json')
        self.assertEqual(response.status_code, 400)
