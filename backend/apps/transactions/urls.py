from django.urls import path

from apps.transactions.views import TransactionsView, BalanceView

urlpatterns = [
    path('transactions', TransactionsView.as_view()),
    path('balance', BalanceView.as_view()),
]
