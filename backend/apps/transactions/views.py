from rest_framework import generics
from apps.transactions.models import Transaction
from apps.transactions.serializer import TransactionSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings

LIM = settings.LIST_LIMIT


class TransactionsView(generics.ListCreateAPIView):
    manager = Transaction.objects
    serializer_class = TransactionSerializer

    def post(self, request):
        try:
            serializer = TransactionSerializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                self.manager.check_valid(serializer.instantiate())
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({"errors": str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def list(self, request):
        try:
            limit = min(int(request.query_params.get('limit', LIM)), LIM)
            page = int(request.query_params.get('page', 1))
            transactions, total, total_pages = self.manager.list(page, limit)
            serializer = TransactionSerializer(transactions, many=True)
            return Response({'total': total, 'total_pages': total_pages, 'transactions': serializer.data}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"errors": str(e)}, status=status.HTTP_400_BAD_REQUEST)


class BalanceView(APIView):
    manager = Transaction.objects
    serializer_class = TransactionSerializer

    def get(self, request):
        balance = self.manager.get_balance()
        return Response({'balance': balance})
