from django.db import models, transaction
from rest_framework import serializers
from apps.transactions.models import Transaction


class TransactionSerializer(serializers.Serializer):
    t_type = serializers.CharField(write_only=True)
    type = serializers.CharField(read_only=True, source='t_type')
    amount = serializers.FloatField()
    id = serializers.IntegerField(read_only=True)
    description = serializers.CharField(required=False)
    createdAt = serializers.DateTimeField(read_only=True, source="created_at")

    class Meta:
        model = Transaction
        fields = ['t_type', 'type', 'amount',
                  'id', 'description', 'createdAt']

    def instantiate(self):
        inst = self.Meta.model(**self.validated_data)
        return inst

    @transaction.atomic
    def create(self, validated_data):
        instance = self.Meta.model(**validated_data)
        instance.save()
        return instance