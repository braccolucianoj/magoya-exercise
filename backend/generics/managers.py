from django.db import connection
from django.db import models

class GenericManager(models.Manager):

    def truncate(self):
        cursor = connection.cursor()
        cursor.execute(f"TRUNCATE TABLE {self.model._meta.db_table}")
