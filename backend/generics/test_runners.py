from django.test.runner import DiscoverRunner

class NoDbTestRunner(DiscoverRunner):

    def setup_databases(self, **kwargs):
        pass
        """ Override the database creation defined in parent class """

    def teardown_databases(self, old_config, **kwargs):
        """ Override the database teardown defined in parent class """
        pass
